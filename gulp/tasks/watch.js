const gulp = require('gulp'),
    del = require('del'),
    browserSync = require('browser-sync').create();

gulp.task('clean', () => {
    return del([
        'temp/styles/main.css',
    ]);
});

gulp.task('cssInject', function () {
    return gulp.src('./app/temp/styles/main.css')
        .pipe(browserSync.stream());
});


gulp.task("watch", () => {
    browserSync.init({
        notify: false,
        server: {
            baseDir: "app"
        }
    });
    gulp.watch('./app/index.html').on('change', browserSync.reload)
    gulp.watch('./app/assets/sass/**/*.scss', gulp.series(['clean', 'styles', 'cssInject']));
    gulp.watch('./app/assets/scripts/**/*.js', gulp.series(['scripts'])).on('change', browserSync.reload);
})